# Static Crypto

Welcome in 2000! What a wonderful time, static pages on Internet Explorer is all you need to be a part of web community.

React, Vue, Svelte? Nah never heard of that! You better focus on HTML and CSS those are all you need here!

## Brief

We have some crazy new client who is interested in crypto. He wants us to make him a static website with all info about those cryptos.

btw. what is this crypto?

The weirdo client wants to have:

- landingpage with all necesery info about crypto,
- subpage about bitcoin, ethereum, polygon

Pages should contain same nav and footer. The only differences are that active page should be hihglighted in the nav.

## Mockups

All the mockups are attached in the repo. Please feel free to adjust style to your project, just keep the structure.

## Tech

Please remember it is only a HTML and CSS project so don't use JS or SCSS/LESS for this project.

**GL&HF**

# Part 2

## In the next iteration:

- [ ] Close all content in 1100px wide container that will always be centered.

- [ ] You convert CSS files to Sass and more specifically SCSS (you will need to do this compiler, compile, compile, [[VSC Extension|https://marketplace.visualstudio.com/items?itemName=MohammadMD.vscode-sassc]])

- [ ] Every HTML file should have seperate SCSS file

- [ ] Create a file with variables, where you declare 3 sizes of fonts and colors that you want to use (you will remain using the code).

- [ ] The entire layout of the page is to be based on flex or grid (IMO flex is better)

- [ ] The navigation bar can have horizontal elements

**GL&HF**
